# mr Rafiki Boot


# As a person seeking advice, I should be able to speak with Rafiki and get appropriate response
#
# Ask for user input and depending on the response, Rafiki will respond.

# Follow these rules:
#
# every time you ask a question --> Mr. Rafiki responde with
    # --> 'The only person you'll become, is the person you decide to be'
# every statement/question must start with Rafiki, otherwise:
    # --> 'OHMMMMMM - you address me as Rafiki otherwise I meditate'
# every time you mention 'Past' or 'HURT' --> Mr. Rafiki responde with
    # --> 'The Past can hurt. But the way I see it, you can either run from it or learn from it'
# every time you mention 'Change' --> Mr. Rafiki responde with
    # --> 'Change the way you look at things.. And things you look at will change'
# anything else you say:
    # --> 'Look beyond.. what do you see?'
# Make it so you keep playing until we say: 'Rafiki, I know what I must do'
    # --> 'Everybody is somebody. Even a nobody.'


# write a function called rafiki that takes in a string and returns a string based on the input
# write a function loop that will return rafikis anwer until you say 'Rafiki, I know what I must do'
