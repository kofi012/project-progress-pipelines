import rafiki_bot
 
def test_rafiki(capsys):
    output = rafiki_bot.rafiki("help")
    output,err = capsys.readouterr()
    assert output == "OHMMMMMM - you address me as Rafiki otherwise I meditate\n"

def test_rafiki_q(capsys):
    output = rafiki_bot.rafiki("rafiki help?")
    output,err = capsys.readouterr()
    assert output == "The only person you'll become, is the person you decide to be\n"

def test_rafiki_hurt(capsys):
    output = rafiki_bot.rafiki("rafiki i am hurt")
    output,err = capsys.readouterr()
    assert output == "The Past can hurt. But the way I see it, you can either run from it or learn from it\n"

def test_rafiki_change(capsys):
    output = rafiki_bot.rafiki("rafiki how can i change")
    output,err = capsys.readouterr()
    assert output == "Change the way you look at things.. And things you look at will change\n"

def test_rafiki_change_past(capsys):
    output = rafiki_bot.rafiki("rafiki my past is long")
    output,err = capsys.readouterr()
    assert output == "The Past can hurt. But the way I see it, you can either run from it or learn from it\n"

def test_rafiki_change_past(capsys):
    output = rafiki_bot.rafiki("rafiki anything else")
    output,err = capsys.readouterr()
    assert output == "Look beyond.. what do you see?\n"