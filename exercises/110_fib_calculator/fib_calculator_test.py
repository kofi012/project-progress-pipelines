import fib_calculator
 
  
def test_calc_fib():
    output = fib_calculator.fibonacci(2)
    assert output == 1
    
def test_calc_fib_3():
    output = fib_calculator.fibonacci(3)
    assert output == 1

def test_calc_fib_10():
    output = fib_calculator.fibonacci(10)
    assert output == 34

def test_calc_fib_1():
    output = fib_calculator.fibonacci(1)
    assert output == 0


     