import age_rights
 
  
def test_age_rights_12():
    output = age_rights.my_rights(12)
    assert output == "You're too young, go back to school!"
    
def test_age_rights_16():
    output = age_rights.my_rights(16)
    assert output == "You can vote"

def test_age_rights_17():
    output = age_rights.my_rights(17)
    assert output == "You can vote and drive"

def test_age_rights_21():
    output = age_rights.my_rights(21)
    assert output == "You can vote and drive and drink"



     