import guess_number
 
  
def test_guess_number_l():
    output = guess_number.guess_number(1,2)
    assert output == "this number is too large"
    
def test_guess_number_s():
    output = guess_number.guess_number(2,1)
    assert output == "this number is too small"

def test_guess_number_e():
    output = guess_number.guess_number(2,2)
    assert output == "this number is the same"



     