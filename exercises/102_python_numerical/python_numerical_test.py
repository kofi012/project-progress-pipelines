import python_numerical
 
def test_square():
    output = python_numerical.calc_square(3,4)
    assert output == 12

def test_cube():
    output = python_numerical.calc_cube(3,4,2)
    assert output == 24